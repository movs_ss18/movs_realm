package com.example.steffen.movs_realm;

import io.realm.RealmObject;

/**
 * Created by Steffen on 02.03.2017.
 */

public class City extends RealmObject {

    private String name;
    private int population;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}
