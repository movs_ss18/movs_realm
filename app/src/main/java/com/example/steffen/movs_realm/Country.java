package com.example.steffen.movs_realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Steffen on 02.03.2017.
 */

public class Country extends RealmObject {

    @PrimaryKey
    private String name;

    private int population;
    private City capital;
    private boolean euCountry;

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Country(){ }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public City getCapital() {
        return capital;
    }

    public void setCapital(City capital) {
        this.capital = capital;
    }

    //Should be removed when removing this value with migration
    public boolean isEuCountry() {
        return euCountry;
    }

    public void setEuCountry(boolean euCountry) {
        this.euCountry = euCountry;
    }

}
