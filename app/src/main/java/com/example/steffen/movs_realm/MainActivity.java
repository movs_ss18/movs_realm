package com.example.steffen.movs_realm;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.exceptions.RealmPrimaryKeyConstraintException;

public class MainActivity extends AppCompatActivity {

    Context c;
    final String COUNTRY_REALM = "countries.realm";
    Realm r_countries;
    EditText et_con, et_cop, et_cin, et_cip;
    Switch sw_eu;
    Button bt_add, bt_1, bt_2, bt_3, bt_4, bt_5;
    TextView tv_result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        c = this;

        et_con = (EditText) findViewById(R.id.et_con);
        et_cop = (EditText) findViewById(R.id.et_cop);
        et_cin = (EditText) findViewById(R.id.et_cin);
        et_cip = (EditText) findViewById(R.id.et_cip);

        sw_eu = (Switch) findViewById(R.id.sw_eu);
        tv_result = (TextView) findViewById(R.id.tv_results);

        bt_add = (Button) findViewById(R.id.bt_add);
        bt_1 = (Button) findViewById(R.id.button4);
        bt_2 = (Button) findViewById(R.id.button5);
        bt_3 = (Button) findViewById(R.id.button6);
        bt_4 = (Button) findViewById(R.id.button7);
        bt_5 = (Button) findViewById(R.id.button8);

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                City ci = new City();
                ci.setPopulation(Integer.parseInt(et_cip.getText().toString()));
                ci.setName(et_cin.getText().toString());

                Country co = new Country();
                co.setPopulation(Integer.parseInt(et_cop.getText().toString()));
                co.setName(et_con.getText().toString());
                co.setCapital(ci);

                addCountry(co);
            }
        });

        bt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllCountries(false);
            }
        });

        bt_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllCountries(true);
            }
        });

        bt_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWorldPop();
            }
        });

        bt_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBigPop();
            }
        });

        bt_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFirst();
            }
        });


        r_countries = buildRealm(COUNTRY_REALM);

    }

    public Realm buildRealm(String name){
        Realm.init(c);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(name)
                .schemaVersion(1)
                .migration(new Migration())
                .build();


        Realm output = Realm.getInstance(config);

        return output;
    }

    public void addCountry(Country co){

        Toast.makeText(c, "Try to add " + co.getName(), Toast.LENGTH_SHORT).show();
        try {
            r_countries.beginTransaction();
            Country copyOfCountry2 = r_countries.copyToRealm(co);
            r_countries.commitTransaction();
        } catch (RealmPrimaryKeyConstraintException e) {
            Toast.makeText(c, co.getName() + " already exists.", Toast.LENGTH_SHORT).show();
        }
    }


    public void getAllCountries(Boolean withCaptial) {
            RealmResults<Country> results1 = r_countries.where(Country.class).findAll();

            tv_result.setText("");
            if (results1.size() == 0){
                tv_result.setText("No results");
            }

            for(Country c:results1) {
                if (withCaptial) {
                    tv_result.append(c.getName() + ", Capital: " + c.getCapital().getName() + ", " + c.getId() + "\n");
                } else {
                    tv_result.append(c.getName() + "\n");
                }
            }
    }


    public void getWorldPop() {
        RealmResults<Country> results2 = r_countries.where(Country.class).findAll();

        tv_result.setText("");
        if (results2.size() == 0){
            tv_result.setText("No results");
        }

        int pop = 0;
        for(Country c:results2) {
            pop = pop + c.getPopulation();
        }
        tv_result.append("World population: " + pop + "\n");
    }


    public void getBigPop() {
        RealmResults<Country> results2 = r_countries.where(Country.class)
                                                    .greaterThan("population", 10000000)
                                                    .findAllSorted("population", Sort.ASCENDING);

        tv_result.setText("");
        if (results2.size() == 0){
            tv_result.setText("No results");
        }

        for(Country c:results2) {
            tv_result.append(c.getName() + ", Population: " + c.getPopulation() + "\n");
        }
    }


    public void deleteFirst() {
        final RealmResults<Country> results1 = r_countries.where(Country.class).findAll();

        tv_result.setText("");
        if (results1.size() == 0){
            tv_result.setText("Nothing to delete");
        } else {

            Country co = results1.get(0);
            String name = co.getName();
            r_countries.beginTransaction();
            co.deleteFromRealm();
            r_countries.commitTransaction();
            tv_result.setText(name + " removed.");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        r_countries.close();
    }
}
